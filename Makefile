.PHONY:	build
build:
	mkdir -p data/ipa-cluster
	python -m scripts.join -p data/temp/ipa.tsv -t data/deps/wordsenses.tsv | sort -h | uniq > data/temp/out.tsv
	python -m scripts.count_translations --cutoff 20 data/temp/out.tsv > data/temp/counts.tsv
	rm -rf data/ipa-cluster
	python -m scripts.api --concepts data/temp/counts.tsv --from data/temp/out.tsv -o data/ipa-cluster
	python -m scripts.index --concepts data/temp/counts.tsv --from data/temp/out.tsv > data/ipa-cluster/index.json

# Run this before `build`.
.PHONY:	prepare
prepare:
	cd data; tar -xjvf deps.tar.bz2
	mkdir -p data/temp
	cat data/deps/broad.tsv data/deps/narrow.tsv | python -m scripts.ipa -s data/temp/usage.tsv > data/temp/ipa.tsv

# Run this after modifying contents of data/deps to save data dependencies.
.PHONY:	update
update:
	cd data; tar -cjvf deps.tar.bz2.test deps

# Check code.
.PHONY:	check
check:
	pylint scripts
	flake8 scripts
	mypy --strict scripts

.PHONY:	dist
dist:
	test -d data/ipa-cluster
	cp -r LICENSES README.md data/ipa-cluster
	cd data; tar -cjvf ipa-cluster.tar.bz2 ipa-cluster
	@echo "See data/ipa-cluster.tar.bz2"
