# ipa-cluster-data

Dataset for [ipa-cluster](https://github.com/palasimi/ipa-cluster).

[Download](https://gitlab.com/palasimi/ipa-cluster-data/-/releases)

## Building the dataset

```bash
# Clone the repository.
git clone https://gitlab.com/palasimi/ipa-cluster-data.git
cd ipa-cluster-data

# Install requirements.
python -m venv env
pip install -r requirements.txt

# Build (this can take up to a few minutes).
make prepare
make build

# See `data/ipa-cluster`.
```

## Licenses

Copyright 2023 Levi Gruspe

The scripts in this repository are licensed under [GPLv3 or later](./LICENSES/GNU_GPLv3.txt).
The dataset is released under a [Creative Commons Attribution-ShareAlike 3.0 Unported License](./LICENSES/CC_BY-SA_3.0.txt).
The data is extracted from Wiktionary with the help of [wiktextract](https://github.com/tatuylonen/wiktextract) and [kaikki.org](https://kaikki.org/index.html).

Wiktionary is licensed under [CC BY-SA 3.0](https://en.wiktionary.org/wiki/Wiktionary:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License).
