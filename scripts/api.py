# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Levi Gruspe
# pylint: disable=duplicate-code
"""Generate static JSON API."""

from argparse import ArgumentParser, Namespace
from csv import reader
from json import dumps
from pathlib import Path
from shutil import move
import sys
from tempfile import TemporaryDirectory
import typing as t

from scripts.bins import compute_bin


class Translation(t.TypedDict):
    """Represents a translation of a concept."""
    language: str
    translation: str
    ipa: str


class Sense(t.TypedDict):
    """Word-sense data with translations."""
    id: str
    word: str
    sense: str
    translations: list[Translation]


def get_concepts(path: Path) -> set[str]:
    """Get set of concepts to include.

    The return value is a set of IDs.
    """
    with open(path, encoding="utf-8") as file:
        rows = reader(file, delimiter="\t")
        assert next(rows) == ["concept", "count"]
        return {concept for concept, _ in rows}


def group_translations_by_concept(
    path: Path,
    concepts: set[str],
) -> dict[str, Sense]:
    """Group translations by concept.

    `path`: path of file to get translations from.
    `concepts`: set of concepts (IDs) to include
    """
    result: dict[str, Sense] = {}
    with open(path, encoding="utf-8") as file:
        rows = reader(file, delimiter="\t")
        for id_, word, sense, translation, language, ipa in rows:
            # Skip concepts that are not in the concept list.
            if id_ not in concepts:
                continue

            value: Sense = {
                "id": id_,
                "word": word,
                "sense": sense,
                "translations": [],
            }
            value = result.setdefault(id_, value)
            value["translations"].append({
                "language": language,
                "translation": translation,
                "ipa": ipa,
            })
    return result


def pack_bins(concepts: dict[str, Sense]) -> dict[str, list[Sense]]:
    """Pack concepts into bins.

    The return value is a dict that maps bin IDs to an array of concepts.
    """
    bins: dict[str, list[Sense]] = {}
    for id_, concept in concepts.items():
        # Exclude untranslated concepts.
        if not concept["translations"]:
            continue

        key = compute_bin(id_)
        bins.setdefault(key, []).append(concept)
    return bins


def parse_args() -> Namespace:
    """Parse command-line arguments."""
    parser = ArgumentParser(description=__doc__)
    parser.add_argument(
        "-c",
        "--concepts",
        dest="concepts",
        required=True,
        type=Path,
        help=(
            "path to list of concepts "
            "(TSV columns: concept ID, number of translations)"
        ),
    )
    parser.add_argument(
        "-f",
        "--from",
        dest="input",
        required=True,
        type=Path,
        help=(
            "path to input file "
            "(TSV columns: sense id, word, sense, translation, language, ipa)"
        ),
    )
    parser.add_argument(
        "-o",
        "--output",
        dest="output",
        required=True,
        type=Path,
        help="output directory; should be empty",
    )
    return parser.parse_args()


def main(args: Namespace) -> None:
    """Script entrypoint."""
    try:
        args.output.mkdir(parents=True, exist_ok=False)
    except FileExistsError:
        sys.exit("output path already exists")

    whitelist = get_concepts(args.concepts)
    concepts = group_translations_by_concept(args.input, whitelist)
    bins = pack_bins(concepts)

    # Write to files.
    with TemporaryDirectory() as tmpdirname:
        parent = Path(tmpdirname)/"temp"
        parent.mkdir(parents=True, exist_ok=True)

        for key, value in bins.items():
            path = parent/f"{key}.json"
            content = dumps(
                {
                    "bin": key,
                    "concepts": value,
                },
                ensure_ascii=False,
            )
            path.write_text(content, encoding="utf-8")

        # `Path.replace` only works if source and target are in the same
        # filesystem, so we use `shutil.move` instead.
        for path in parent.iterdir():
            target = args.output/path.name
            target.unlink(missing_ok=True)
            move(path, target)


if __name__ == "__main__":
    main(parse_args())
