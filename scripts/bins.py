# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Levi Gruspe
"""Pack concepts into bins."""

from math import ceil, log2

from base58 import b58decode, b58encode


# Concept IDs are sufficiently evenly distributed mod 8192.
# See `distributions.py`.
MODULUS = 8192

# Number of bytes in bin ID.
LENGTH = ceil(log2(MODULUS) / 8)


def compute_bin(concept: str) -> str:
    """Compute bin ID of concept."""
    # Bin ID is just the remainder of the concept ID.
    # The number of translations for the given concept isn't considered.
    bin_ = int.from_bytes(b58decode(concept.encode())) % MODULUS

    # Encode in base58.
    return b58encode(int.to_bytes(bin_, length=LENGTH)).decode()


__all__ = ["compute_bin"]
