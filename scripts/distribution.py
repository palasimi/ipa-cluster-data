# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Levi Gruspe
"""Analyze distribution of concept IDs."""

from argparse import ArgumentParser, Namespace
from collections import Counter
from csv import reader
from pathlib import Path

from base58 import b58decode


def parse_args() -> Namespace:
    """Parse command-line arguments."""
    parser = ArgumentParser(description=__doc__)
    parser.add_argument(
        "-c",
        "--concepts",
        dest="concepts",
        required=True,
        type=Path,
        help=(
            "path to list of concepts "
            "(TSV columns: concept ID, number of translations)"
        ),
    )
    parser.add_argument(
        "-m",
        "--mod",
        "--modulus",
        dest="modulus",
        default=8192,
        type=int,
        help="number of bins among which to distribute the concepts",
    )
    return parser.parse_args()


def main(args: Namespace) -> None:
    """Script entrypoint."""
    # Get list of concepts.
    with open(args.concepts, encoding="utf-8") as file:
        rows = reader(file, delimiter="\t")
        assert next(rows) == ["concept", "count"]
        concepts = {concept for concept, _ in rows}

    # Analyze distribution.
    counter: Counter[int] = Counter()
    for concept in concepts:
        bin_ = int.from_bytes(b58decode(concept.encode())) % args.modulus
        counter[bin_] += 1

    distribution: Counter[int] = Counter()
    for _, count in counter.most_common():
        distribution[count] += 1

    mean = 0.0
    total = 0
    for size, count in distribution.most_common():
        print(size, count)
        mean += size * count
        total += count
    mean /= total

    # Print average.
    print("average:", mean)
    print("bins:", total)


if __name__ == "__main__":
    main(parse_args())
