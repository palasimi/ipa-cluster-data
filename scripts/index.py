# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Levi Gruspe
# pylint: disable=duplicate-code
"""Generate search index for word senses."""

from argparse import ArgumentParser, Namespace
from csv import reader
from json import dumps
from pathlib import Path

from scripts.bins import compute_bin


def parse_args() -> Namespace:
    """Parse command-line arguments."""
    parser = ArgumentParser(description=__doc__)
    parser.add_argument(
        "-c",
        "--concepts",
        dest="concepts",
        required=True,
        type=Path,
        help=(
            "path to list of concepts "
            "(TSV columns: concept ID, number of translations)"
        ),
    )
    parser.add_argument(
        "-f",
        "--from",
        dest="input",
        required=True,
        type=Path,
        help=(
            "path to input file "
            "(TSV columns: sense id, word, sense, translation, language, ipa)"
        ),
    )
    return parser.parse_args()


def main(args: Namespace) -> None:
    """Script entrypoint."""
    # Get concepts to include.
    with open(args.concepts, encoding="utf-8") as file:
        rows = reader(file, delimiter="\t")
        assert next(rows) == ["concept", "count"]
        concepts = {concept for concept, _ in rows}

    # Get word senses.
    word_senses: set[tuple[str, str, str]] = set()
    with open(args.input, encoding="utf-8") as file:
        for row in reader(file, delimiter="\t"):
            id_ = row[0]
            if id_ not in concepts:
                continue
            word = row[1]
            sense = row[2]
            word_senses.add((id_, word, sense))

    # Construct index.
    index = [
        {"id": id_, "word": word, "sense": sense, "bin": compute_bin(id_)}
        for id_, word, sense in word_senses
    ]

    dump = dumps({"index": index}, ensure_ascii=False)
    print(dump)


if __name__ == "__main__":
    main(parse_args())
