# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Levi Gruspe
"""Count number of translations of each concept."""

from argparse import ArgumentParser, Namespace
from collections import Counter
from csv import reader, writer
from pathlib import Path
import sys
import typing as t


Concept: t.TypeAlias = str
Language: t.TypeAlias = str
Word: t.TypeAlias = str
Translation: t.TypeAlias = tuple[Language, Word]


def parse_args() -> Namespace:
    """Parse command-line arguments."""
    parser = ArgumentParser(description=__doc__)
    parser.add_argument(
        "-c",
        "--cutoff",
        dest="cutoff",
        default=0,
        type=int,
        help="minimum number of translations needed to include concept",
    )
    parser.add_argument(
        "input",
        type=Path,
        help=(
            "path to input file "
            "(TSV columns: sense id, word, sense, translation, language, ipa)"
        ),
    )
    return parser.parse_args()


def main(args: Namespace) -> None:
    """Script entrypoint."""
    # Get translations of each concept.
    translations: dict[Concept, set[Translation]] = {}
    with open(args.input, encoding="utf-8") as file:
        for row in reader(file, delimiter="\t"):
            id_ = row[0]
            language = row[4]
            word = row[3]

            translation = (language, word)
            translations.setdefault(id_, set()).add(translation)

    # Count translations.
    counter: Counter[Concept] = Counter()
    for concept, translations_ in translations.items():
        count = len(translations_)
        if count >= args.cutoff:
            counter[concept] = count

    # Print result.
    out = writer(sys.stdout, delimiter="\t")
    out.writerow(("concept", "count"))
    out.writerows(counter.most_common())


if __name__ == "__main__":
    main(parse_args())
