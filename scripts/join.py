# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Levi Gruspe
"""Combine translations with IPA data."""

from argparse import ArgumentParser, Namespace
from csv import reader, writer
from pathlib import Path
import sys
import typing as t


Language: t.TypeAlias = str
Word: t.TypeAlias = str
Pronunciation: t.TypeAlias = str


def load_pronunciations(
    path: Path,
) -> dict[tuple[Language, Word], set[Pronunciation]]:
    """Load pronunciations from TSV file."""
    result: dict[tuple[Language, Word], set[Pronunciation]] = {}

    with open(path, encoding="utf-8") as file:
        rows = reader(file, delimiter="\t")
        for language, word, pronunciation in rows:
            key = (language, word)
            result.setdefault(key, set()).add(pronunciation)
    return result


def parse_args() -> Namespace:
    """Parse command-line arguments."""
    parser = ArgumentParser(description=__doc__)
    parser.add_argument(
        "-p",
        "--pronunciations",
        dest="pronunciations",
        required=True,
        type=Path,
        help=(
            "path to pronunciations TSV file "
            "(columns: language code, translation, pronunciation)"
        ),
    )
    parser.add_argument(
        "-t",
        "--translations",
        dest="translations",
        required=True,
        type=Path,
        help=(
            "path to translations TSV file "
            "(columns: language code, translation, concept ID, word, sense)"
        ),
    )
    return parser.parse_args()


def main(args: Namespace) -> None:
    """Script entrypoint."""
    pronunciations = load_pronunciations(args.pronunciations)

    out = writer(sys.stdout, delimiter="\t")
    with open(args.translations, encoding="utf-8") as file:
        rows = reader(file, delimiter="\t")
        for language, translation, id_, word, sense in rows:
            key = (language, translation)
            for pronunciation in pronunciations.get(key, []):
                row = (id_, word, sense, translation, language, pronunciation)
                out.writerow(row)


if __name__ == "__main__":
    main(parse_args())
