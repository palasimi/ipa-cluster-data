# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Levi Gruspe
"""Prepare IPA transcriptions."""

from argparse import ArgumentParser, Namespace
from collections import Counter
from csv import reader, writer
from pathlib import Path
import sys

from ipa_tokenizer.tokenizer import tokenize, UnknownSymbol


def parse_args() -> Namespace:
    """Parse command-line arguments."""
    usage = """python ipa.py < $TSV_FILE > out.tsv

    The TSV file is expected to have three columns: language, word, IPA."""
    parser = ArgumentParser(description=__doc__, usage=usage)
    parser.add_argument(
        "-s",
        "--stats",
        "--statistics",
        dest="statistics",
        default=None,
        type=Path,
        help="output file for IPA symbol usage statistics",
    )
    return parser.parse_args()


def main(args: Namespace) -> None:
    """Script entrypoint."""
    if args.statistics is not None:
        counter: Counter[str] = Counter()

    out = writer(sys.stdout, delimiter="\t")
    for language, word, ipa in reader(sys.stdin, delimiter="\t"):
        # There's no need to check if the transcription starts or ends with a
        # hyphen, because word2ipa already filters out partial transcriptions.

        # Tokenize IPA.
        try:
            tokens = tokenize(ipa, language=language)
        except UnknownSymbol:
            continue
        row = (language, word, " ".join(tokens))
        out.writerow(row)

        if args.statistics is not None:
            counter.update(tokens)

    if args.statistics is not None:
        with open(args.statistics, "w", encoding="utf-8") as file:
            out = writer(file, delimiter="\t")
            for symbol, count in counter.most_common():
                out.writerow((symbol, count))


if __name__ == "__main__":
    main(parse_args())
